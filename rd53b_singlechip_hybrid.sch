EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 2
Title "RD53B/ITkPixV1 SingleChip Hybrid"
Date "2020-07-06"
Rev "1.0"
Comp "LBNL"
Comment1 "cross-check: Timon Heim"
Comment2 "design: Aleksandra Dimitrievska"
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 7900 4800 1200 1400
U 5F03BA43
F0 "rd53b_chip" 50
F1 "rd53b_chip.sch" 50
F2 "CMD_P" I L 7900 4900 50 
F3 "CMD_N" I L 7900 5000 50 
F4 "MUX" I L 7900 5100 50 
F5 "VIN" I L 7900 5200 50 
F6 "GTX0_N" I L 7900 5400 50 
F7 "GTX0_P" I L 7900 5500 50 
F8 "GTX1_N" I L 7900 5600 50 
F9 "GTX1_P" I L 7900 5700 50 
F10 "GTX2_N" I L 7900 5800 50 
F11 "GTX2_P" I L 7900 5900 50 
F12 "GTX3_N" I L 7900 6000 50 
F13 "GTX3_P" I L 7900 6100 50 
F14 "DATA_IN0_P" I R 9100 4950 50 
F15 "DATA_IN0_N" I R 9100 5050 50 
F16 "DATA_IN1_P" I R 9100 5150 50 
F17 "DATA_IN2_P" I R 9100 5350 50 
F18 "DATA_IN3_P" I R 9100 5550 50 
F19 "DATA_IN1_N" I R 9100 5250 50 
F20 "DATA_IN2_N" I R 9100 5450 50 
F21 "DATA_IN3_N" I R 9100 5650 50 
F22 "LVDS0_P" I R 9100 5750 50 
F23 "LVDS1_P" I R 9100 5950 50 
F24 "LVDS0_N" I R 9100 5850 50 
F25 "LVDS1_N" I R 9100 6050 50 
F26 "LP_EN" I L 7900 5300 50 
$EndSheet
Wire Wire Line
	9100 4950 9750 4950
Wire Wire Line
	9100 5050 9750 5050
Wire Wire Line
	9100 5150 9750 5150
Wire Wire Line
	9100 5250 9750 5250
Wire Wire Line
	9100 5350 9750 5350
Wire Wire Line
	9100 5450 9750 5450
Wire Wire Line
	9100 5550 9750 5550
Wire Wire Line
	9100 5650 9750 5650
Wire Wire Line
	9100 5750 9750 5750
Wire Wire Line
	9100 5850 9750 5850
Wire Wire Line
	9100 5950 9750 5950
Wire Wire Line
	9100 6050 9750 6050
Wire Wire Line
	7900 6100 7250 6100
Wire Wire Line
	7900 6000 7250 6000
Wire Wire Line
	7900 5900 7250 5900
Wire Wire Line
	7900 5800 7250 5800
Wire Wire Line
	7900 5700 7250 5700
Wire Wire Line
	7900 5600 7250 5600
Wire Wire Line
	7900 5500 7250 5500
Wire Wire Line
	7900 5400 7250 5400
Wire Wire Line
	7900 5300 7250 5300
Wire Wire Line
	7900 5200 7250 5200
Wire Wire Line
	7900 5100 7250 5100
Wire Wire Line
	7900 5000 7250 5000
Wire Wire Line
	7900 4900 7250 4900
Text Label 7250 4900 0    50   ~ 0
CMD_P
Text Label 7250 5000 0    50   ~ 0
CMD_N
Text Label 7250 5100 0    50   ~ 0
MUX
Text Label 7250 5200 0    50   ~ 0
VIN
Text Label 7250 5300 0    50   ~ 0
LP_EN
Text Label 7250 5400 0    50   ~ 0
GTX0_N
Text Label 7250 5500 0    50   ~ 0
GTX0_P
Text Label 7250 5600 0    50   ~ 0
GTX1_N
Text Label 7250 5700 0    50   ~ 0
GTX1_P
Text Label 7250 5800 0    50   ~ 0
GTX2_N
Text Label 7250 5900 0    50   ~ 0
GTX2_P
Text Label 7250 6000 0    50   ~ 0
GTX3_N
Text Label 7250 6100 0    50   ~ 0
GTX3_P
Text Label 9750 4950 2    50   ~ 0
DATA_IN0_P
Text Label 9750 5050 2    50   ~ 0
DATA_IN0_N
Text Label 9750 5150 2    50   ~ 0
DATA_IN1_P
Text Label 9750 5250 2    50   ~ 0
DATA_IN1_N
Text Label 9750 5350 2    50   ~ 0
DATA_IN2_P
Text Label 9750 5450 2    50   ~ 0
DATA_IN2_N
Text Label 9750 5550 2    50   ~ 0
DATA_IN3_P
Text Label 9750 5650 2    50   ~ 0
DATA_IN3_N
Text Label 9750 5750 2    50   ~ 0
LVDS0_P
Text Label 9750 5850 2    50   ~ 0
LVDS0_N
Text Label 9750 5950 2    50   ~ 0
LVDS1_P
Text Label 9750 6050 2    50   ~ 0
LVDS1_N
Wire Notes Line
	7150 6350 9850 6350
Wire Notes Line
	9850 6350 9850 4650
Wire Notes Line
	9850 4650 7150 4650
Wire Notes Line
	7150 4650 7150 6350
Text Notes 7200 4750 0    50   ~ 0
ITkPixV1 Chip
$Comp
L Molex-502598-3993:502598-3993 J?
U 1 1 5F07DF6A
P 1350 1050
F 0 "J?" V 3354 1478 50  0000 L CNN
F 1 "502598-3993" V 3445 1478 50  0000 L CNN
F 2 "Molex-502598-3993-*" H 1350 1750 50  0001 L CNN
F 3 "http://www.molex.com/webdocs/datasheets/pdf/en-us/5025983993_FFC_FPC_CONNECTORS.pdf" H 1350 1850 50  0001 L CNN
F 4 "39" H 1350 1950 50  0001 L CNN "Circuits Loaded"
F 5 "Manufacturer URL" H 1350 2050 50  0001 L CNN "Component Link 1 Description"
F 6 "http://www.molex.com/molex/index.jsp" H 1350 2150 50  0001 L CNN "Component Link 1 URL"
F 7 "Package Specification" H 1350 2250 50  0001 L CNN "Component Link 3 Description"
F 8 "http://www.molex.com/pdm_docs/sd/5025983993_sd.pdf" H 1350 2350 50  0001 L CNN "Component Link 3 URL"
F 9 "Dual" H 1350 2450 50  0001 L CNN "Contact Position"
F 10 "0.2A" H 1350 2550 50  0001 L CNN "Current Max per Contact"
F 11 "10" H 1350 2650 50  0001 L CNN "Durability mating cycles max"
F 12 "90degrees Angle" H 1350 2750 50  0001 L CNN "Entry Angle"
F 13 "1.15mm" H 1350 2850 50  0001 L CNN "Mated Height"
F 14 "Copper Alloy, Phosphor Bronze" H 1350 2950 50  0001 L CNN "Material   Metal"
F 15 "Gold" H 1350 3050 50  0001 L CNN "Material   Plating Mating"
F 16 "Gold, Tin" H 1350 3150 50  0001 L CNN "Material   Plating Termination"
F 17 "Surface Mount" H 1350 3250 50  0001 L CNN "Mounting Technology"
F 18 "1" H 1350 3350 50  0001 L CNN "Number of Rows"
F 19 "Right Angle" H 1350 3450 50  0001 L CNN "Orientation"
F 20 "No" H 1350 3550 50  0001 L CNN "PCB Locator"
F 21 "Yes" H 1350 3650 50  0001 L CNN "PCB Retention"
F 22 "39-Lead FPC Connector, Pitch 0.3 mm" H 1350 3750 50  0001 L CNN "Package Description"
F 23 "Rev. B, 08/2013" H 1350 3850 50  0001 L CNN "Package Version"
F 24 "Tape and Reel" H 1350 3950 50  0001 L CNN "Packing"
F 25 "0.30mm" H 1350 4050 50  0001 L CNN "Pitch   Mating Interface"
F 26 "Yes" H 1350 4150 50  0001 L CNN "Polarized to PCB"
F 27 "No" H 1350 4250 50  0001 L CNN "Stackable"
F 28 "50V AC (RMS)/DC" H 1350 4350 50  0001 L CNN "Voltage Max"
F 29 "Conn" H 1350 4450 50  0001 L CNN "category"
F 30 "8412423" H 1350 4550 50  0001 L CNN "ciiva ids"
F 31 "5b17d0d47d59ca2c" H 1350 4650 50  0001 L CNN "library id"
F 32 "Molex" H 1350 4750 50  0001 L CNN "manufacturer"
F 33 "502598-3993" H 1350 4850 50  0001 L CNN "package"
F 34 "1411372321" H 1350 4950 50  0001 L CNN "release date"
F 35 "Yes" H 1350 5050 50  0001 L CNN "rohs"
F 36 "6F5A5FE4-A61E-42AA-9E0C-7169902D2A67" H 1350 5150 50  0001 L CNN "vault revision"
F 37 "yes" H 1350 5250 50  0001 L CNN "imported"
	1    1350 1050
	0    1    1    0   
$EndComp
Wire Wire Line
	1450 1050 800  1050
Wire Wire Line
	1450 1150 800  1150
Wire Wire Line
	1450 1250 800  1250
Wire Wire Line
	1450 1350 800  1350
Wire Wire Line
	1450 1450 800  1450
Wire Wire Line
	1450 1550 800  1550
Wire Wire Line
	1450 1650 800  1650
Wire Wire Line
	1450 1750 800  1750
Wire Wire Line
	1450 1850 800  1850
Wire Wire Line
	1450 1950 800  1950
Wire Wire Line
	1450 2050 800  2050
Wire Wire Line
	1450 2150 800  2150
Wire Wire Line
	1450 2250 800  2250
Wire Wire Line
	1450 2350 800  2350
Wire Wire Line
	1450 2450 800  2450
Wire Wire Line
	1450 2550 800  2550
Wire Wire Line
	1450 2650 800  2650
Wire Wire Line
	1450 2750 800  2750
Wire Wire Line
	1450 2850 800  2850
Wire Wire Line
	1450 2950 800  2950
Wire Wire Line
	1450 3050 800  3050
Wire Wire Line
	1450 3150 800  3150
Wire Wire Line
	1450 3250 800  3250
Wire Wire Line
	1450 3350 800  3350
Wire Wire Line
	1450 3450 800  3450
Wire Wire Line
	1450 3550 800  3550
Wire Wire Line
	1450 3650 800  3650
Wire Wire Line
	1450 3750 800  3750
Wire Wire Line
	1450 3850 800  3850
Wire Wire Line
	1450 3950 800  3950
Wire Wire Line
	1450 4050 800  4050
Wire Wire Line
	1450 4150 800  4150
Wire Wire Line
	1450 4250 800  4250
Wire Wire Line
	1450 4350 800  4350
Wire Wire Line
	1450 4450 800  4450
Wire Wire Line
	1450 4550 800  4550
Wire Wire Line
	1450 4650 800  4650
Wire Wire Line
	1450 4750 800  4750
Wire Wire Line
	1450 4850 800  4850
Wire Wire Line
	1450 5050 800  5050
Wire Wire Line
	1450 5150 800  5150
Text Label 800  5150 0    50   ~ 0
GNDM
Text Label 800  5050 0    50   ~ 0
GNDM
$Comp
L power:GND #PWR?
U 1 1 5F098B8E
P 850 5600
F 0 "#PWR?" H 850 5350 50  0001 C CNN
F 1 "GND" H 855 5427 50  0000 C CNN
F 2 "" H 850 5600 50  0001 C CNN
F 3 "" H 850 5600 50  0001 C CNN
	1    850  5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	850  5600 850  5500
Wire Wire Line
	850  5500 1100 5500
$Comp
L Device:C C?
U 1 1 5F09BDF7
P 1250 5500
F 0 "C?" V 1100 5500 50  0000 C CNN
F 1 "100n" V 1400 5500 50  0000 C CNN
F 2 "" H 1288 5350 50  0001 C CNN
F 3 "~" H 1250 5500 50  0001 C CNN
	1    1250 5500
	0    1    1    0   
$EndComp
Wire Wire Line
	1400 5500 1750 5500
Text Label 1750 5500 2    50   ~ 0
GNDM
Wire Notes Line
	650  850  650  5900
Wire Notes Line
	650  5900 2450 5900
Wire Notes Line
	2450 5900 2450 850 
Wire Notes Line
	2450 850  650  850 
Text Notes 700  950  0    50   ~ 0
Data Connector 3*4+3+1+1+3*4+3*2+2+2
Text Notes 3050 950  0    50   ~ 0
LV Power
Text Notes 4300 950  0    50   ~ 0
HV Filter
Text Notes 5350 950  0    50   ~ 0
NTC
Text Notes 6400 950  0    50   ~ 0
CMD
Text Notes 2550 2550 0    50   ~ 0
LP Enable\n
Text Label 2550 3300 0    50   ~ 0
LP_EN_AC
Wire Wire Line
	2550 3300 3200 3300
$Comp
L Device:C C?
U 1 1 5FC05294
P 3350 3300
F 0 "C?" V 3098 3300 50  0000 C CNN
F 1 "100n" V 3189 3300 50  0000 C CNN
F 2 "" H 3388 3150 50  0001 C CNN
F 3 "~" H 3350 3300 50  0001 C CNN
	1    3350 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	3500 3300 3800 3300
Wire Wire Line
	3800 3300 3800 3150
Wire Wire Line
	3800 2850 4450 2850
Wire Wire Line
	3800 3300 3800 3450
Wire Wire Line
	3800 3750 4450 3750
Connection ~ 3800 3300
Wire Wire Line
	3800 3150 4450 3150
Connection ~ 3800 3150
Wire Wire Line
	3800 3150 3800 2850
Wire Wire Line
	3800 3450 4450 3450
Connection ~ 3800 3450
Wire Wire Line
	3800 3450 3800 3750
Text Label 4450 2850 2    50   ~ 0
LP_EN_Chip1
Text Label 4450 3150 2    50   ~ 0
LP_EN_Chip2
Text Label 4450 3450 2    50   ~ 0
LP_EN_Chip3
Text Label 4450 3750 2    50   ~ 0
LP_EN_Chip4
Wire Notes Line
	2500 2450 2500 3850
Wire Notes Line
	2500 3850 4600 3850
Wire Notes Line
	4600 3850 4600 2450
Wire Notes Line
	4600 2450 2500 2450
Text Notes 5850 2500 0    50   ~ 0
VOffset Sharing?
Text Notes 2550 4050 0    50   ~ 0
Terminate Data_In and/or LVDS_Out if not used?\n
Text Label 8250 2650 2    50   ~ 0
VOFS_IN_Chip1
Text Label 8250 2800 2    50   ~ 0
VOFS_IN_Chip2
Text Label 8250 2950 2    50   ~ 0
VOFS_IN_Chip3
Text Label 8250 3100 2    50   ~ 0
VOFS_IN_Chip4
Wire Wire Line
	7500 3350 8250 3350
Text Label 8250 3350 2    50   ~ 0
VOFS_OUT_Chip1
Text Label 8250 3500 2    50   ~ 0
VOFS_OUT_Chip2
Text Label 8250 3650 2    50   ~ 0
VOFS_OUT_Chip3
Text Label 8250 3800 2    50   ~ 0
VOFS_OUT_Chip4
Text Label 8250 4050 2    50   ~ 0
VOFS_LP_Chip1
Text Label 8250 4200 2    50   ~ 0
VOFS_LP_Chip2
Text Label 8250 4350 2    50   ~ 0
VOFS_LP_Chip3
Text Label 8250 4500 2    50   ~ 0
VOFS_LP_Chip4
Wire Wire Line
	9000 2600 9750 2600
Text Label 9750 2600 2    50   ~ 0
VOFS_IN
Wire Wire Line
	9000 2800 9750 2800
Text Label 9750 2800 2    50   ~ 0
VOFS_OUT
Text Label 9750 3000 2    50   ~ 0
VOFS_LP
Wire Wire Line
	9000 2600 9000 2700
$Comp
L Device:R R?
U 1 1 5FC4AC98
P 8700 2700
F 0 "R?" V 8800 2700 50  0000 C CNN
F 1 "24.9k" V 8584 2700 50  0000 C CNN
F 2 "" V 8630 2700 50  0001 C CNN
F 3 "~" H 8700 2700 50  0001 C CNN
	1    8700 2700
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5FC4B408
P 8700 3200
F 0 "R?" V 8800 3200 50  0000 C CNN
F 1 "10k" V 8584 3200 50  0000 C CNN
F 2 "" V 8630 3200 50  0001 C CNN
F 3 "~" H 8700 3200 50  0001 C CNN
	1    8700 3200
	0    1    1    0   
$EndComp
Wire Wire Line
	8850 2700 9000 2700
Connection ~ 9000 2700
Wire Wire Line
	9000 2700 9000 2800
Text Label 9000 2700 0    50   ~ 0
VOFS
Wire Wire Line
	8550 2700 8400 2700
Wire Wire Line
	8400 2700 8400 3000
Wire Wire Line
	8400 3000 9750 3000
Wire Wire Line
	8400 3000 8400 3200
Wire Wire Line
	8400 3200 8550 3200
Connection ~ 8400 3000
Wire Wire Line
	8850 3200 9750 3200
Text Label 9750 3200 2    50   ~ 0
GNDA_REF
Text Notes 8350 2450 0    50   ~ 0
For one Chip with VOFS Sharing\n
$Comp
L Device:R R?
U 1 1 5FC5DCCF
P 6700 4050
F 0 "R?" V 6800 4050 50  0000 C CNN
F 1 "10k" V 6584 4050 50  0000 C CNN
F 2 "" V 6630 4050 50  0001 C CNN
F 3 "~" H 6700 4050 50  0001 C CNN
	1    6700 4050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6550 4050 5650 4050
Text Label 5650 4050 0    50   ~ 0
GNDA_REF_Chip1
$Comp
L Device:R R?
U 1 1 5FC644D7
P 6450 4200
F 0 "R?" V 6550 4200 50  0000 C CNN
F 1 "10k" V 6334 4200 50  0000 C CNN
F 2 "" V 6380 4200 50  0001 C CNN
F 3 "~" H 6450 4200 50  0001 C CNN
	1    6450 4200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6300 4200 5400 4200
Text Label 5400 4200 0    50   ~ 0
GNDA_REF_Chip2
$Comp
L Device:R R?
U 1 1 5FC6BCD0
P 6200 4350
F 0 "R?" V 6300 4350 50  0000 C CNN
F 1 "10k" V 6084 4350 50  0000 C CNN
F 2 "" V 6130 4350 50  0001 C CNN
F 3 "~" H 6200 4350 50  0001 C CNN
	1    6200 4350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6050 4350 5150 4350
Text Label 5150 4350 0    50   ~ 0
GNDA_REF_Chip3
$Comp
L Device:R R?
U 1 1 5FC7102B
P 5950 4500
F 0 "R?" V 6050 4500 50  0000 C CNN
F 1 "10k" V 5834 4500 50  0000 C CNN
F 2 "" V 5880 4500 50  0001 C CNN
F 3 "~" H 5950 4500 50  0001 C CNN
	1    5950 4500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5800 4500 4900 4500
Text Label 4900 4500 0    50   ~ 0
GNDA_REF_Chip4
Wire Wire Line
	6600 4200 7100 4200
Wire Wire Line
	6350 4350 6850 4350
Text Notes 2650 2700 0    50   ~ 0
or AC couple to each chip
$Comp
L Device:R R?
U 1 1 5FC8B380
P 7350 3350
F 0 "R?" V 7450 3350 50  0000 C CNN
F 1 "24.9k" V 7234 3350 50  0000 C CNN
F 2 "" V 7280 3350 50  0001 C CNN
F 3 "~" H 7350 3350 50  0001 C CNN
	1    7350 3350
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5FC8BDFB
P 7100 3500
F 0 "R?" V 7200 3500 50  0000 C CNN
F 1 "24.9k" V 6984 3500 50  0000 C CNN
F 2 "" V 7030 3500 50  0001 C CNN
F 3 "~" H 7100 3500 50  0001 C CNN
	1    7100 3500
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5FC8C5F6
P 6850 3650
F 0 "R?" V 6950 3650 50  0000 C CNN
F 1 "24.9k" V 6734 3650 50  0000 C CNN
F 2 "" V 6780 3650 50  0001 C CNN
F 3 "~" H 6850 3650 50  0001 C CNN
	1    6850 3650
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5FC8CB51
P 6600 3800
F 0 "R?" V 6700 3800 50  0000 C CNN
F 1 "24.9k" V 6484 3800 50  0000 C CNN
F 2 "" V 6530 3800 50  0001 C CNN
F 3 "~" H 6600 3800 50  0001 C CNN
	1    6600 3800
	0    1    1    0   
$EndComp
Wire Wire Line
	7250 3500 8250 3500
Wire Wire Line
	7000 3650 8250 3650
Wire Wire Line
	6750 3800 8250 3800
Wire Wire Line
	6450 3800 6050 3800
Wire Wire Line
	6050 3800 6050 3650
Wire Wire Line
	6050 3350 7200 3350
Wire Wire Line
	6950 3500 6050 3500
Connection ~ 6050 3500
Wire Wire Line
	6050 3500 6050 3350
Wire Wire Line
	6700 3650 6050 3650
Connection ~ 6050 3650
Wire Wire Line
	6050 3650 6050 3500
Wire Wire Line
	6050 3350 6050 3200
Wire Wire Line
	6050 2650 8250 2650
Connection ~ 6050 3350
Wire Wire Line
	6050 2800 8250 2800
Connection ~ 6050 2800
Wire Wire Line
	6050 2800 6050 2650
Wire Wire Line
	6050 2950 8250 2950
Connection ~ 6050 2950
Wire Wire Line
	6050 2950 6050 2800
Wire Wire Line
	6050 3100 8250 3100
Connection ~ 6050 3100
Wire Wire Line
	6050 3100 6050 2950
Wire Wire Line
	6050 3200 5600 3200
Connection ~ 6050 3200
Wire Wire Line
	6050 3200 6050 3100
Text Label 5600 3200 0    50   ~ 0
VOFS_HALF
Wire Wire Line
	6850 4050 7300 4050
Text Label 6550 4450 0    50   ~ 0
VOFS_HALF
Wire Wire Line
	6100 4500 6550 4500
Text Label 6850 4300 0    50   ~ 0
VOFS_HALF
Text Label 7100 4150 0    50   ~ 0
VOFS_HALF
Text Label 7300 4000 0    50   ~ 0
VOFS_HALF
Wire Wire Line
	7300 4000 7300 4050
Connection ~ 7300 4050
Wire Wire Line
	7300 4050 8250 4050
Wire Wire Line
	7100 4150 7100 4200
Connection ~ 7100 4200
Wire Wire Line
	7100 4200 8250 4200
Wire Wire Line
	6850 4300 6850 4350
Connection ~ 6850 4350
Wire Wire Line
	6850 4350 8250 4350
Wire Wire Line
	6550 4450 6550 4500
Connection ~ 6550 4500
Wire Wire Line
	6550 4500 8250 4500
Text Notes 5050 4150 0    50   ~ 0
or GND?
$EndSCHEMATC
